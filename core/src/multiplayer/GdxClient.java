package multiplayer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import network.Connection;
import network.Messager;
import network.NetworkCons;
import network.messages.BalanceMessage;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;

public class GdxClient extends Connection
{

	private String serverUrl;
	private int timeout;
	private int tcpPort;
	private Socket socket;
	//	protected BufferedReader buffer;
	private DataOutputStream dOut;
	private DataInputStream dIn;

	public GdxClient(Messager messager)
	{
		super(messager);

		this.serverUrl = NetworkCons.getLocalhost();
		this.timeout = 0;
		this.tcpPort = NetworkCons.getTcpPort();
	}

	public GdxClient(Messager messager, String serverUrl, int timeOut, int tcpPort)
	{
		super(messager);

		this.serverUrl = serverUrl;
		this.timeout = timeOut;
		this.tcpPort = tcpPort;
	}

	@Override
	public boolean connectUdp()
	{
		throw new NotImplementedException(); 

	}

	@Override
	public void connectTcp()
	{

		SocketHints socketHints = new SocketHints();
		// Socket will time our in 4 seconds
		socketHints.connectTimeout = this.timeout;
		//create the socket and connect to the server entered in the text box ( x.x.x.x format ) on port 9021
		socket = Gdx.net.newClientSocket(Protocol.TCP, this.serverUrl, this.tcpPort, socketHints);
		dOut = new DataOutputStream(socket.getOutputStream());
		dIn = new DataInputStream(socket.getInputStream());


		// listening thread
		new Thread (new Runnable()
		{

			@Override
			public void run()
			{
				try 
				{
					while (true)
					{
						readInput();
					}
					/*dOut.close();
					dOut.close();
					dIn.close();
					socket.dispose();*/
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}

			private void readInput() throws IOException
			{
				ByteBuffer headerBuffer = ByteBuffer.allocate(NetworkCons.getHeaderLength());
				int bytesRead = dIn.read(headerBuffer.array());
				int messageLength = headerBuffer.getInt(); 
				ByteBuffer messageBuffer = ByteBuffer.allocate(messageLength); 
				dIn.readFully(messageBuffer.array());
				headerBuffer.flip();
				headerBuffer.clear();
				messageBuffer.flip();
				messager.add(headerBuffer, messageBuffer);
			}
		}).start();
	}

	@Override
	public void sendMessage(String message)
	{

		try
		{
			dOut.writeInt(message.length());
			System.out.println("Length send :" + message.length());

			// TODO check if can replace with "UTF-8" ?
			String messageUTF = new String( message.getBytes(), Charset.forName("UTF-8") );
			dOut.write(messageUTF.getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(byte[] message)
	{
		try
		{				
			dOut.write(message.length);
			dOut.write(message);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void disconnect()
	{
		sendMessage("END");
		System.out.println("Client disconnects");

	}

	@Override
	public void sendMessage(BalanceMessage message)
	{
		try
		{
			ByteBuffer arr = message.serializeMessage();
			dOut.write(arr.array());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

}
