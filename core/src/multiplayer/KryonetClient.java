//package multiplayer;
//
//import java.io.IOException;
//
//import com.esotericsoftware.kryonet.Client;
//import com.esotericsoftware.kryonet.Connection;
//import com.esotericsoftware.kryonet.Listener;
//import com.wecanbalance.duplicate.multiplayer.NetworkCons;
//import com.wecanbalance.duplicate.multiplayer.PacketMessage;
//
//public class KryonetClient implements ClientI
//{
//	
//	private int tcpPort;
//	private int udpPort;
//	private int timeoutLimit;
//	private boolean isMessageReceived;
//	private Client client;
//	private String url;
//
//	public KryonetClient(String url,int timeoutLimit)
//	{
//		this.url = url;
//		this.timeoutLimit = timeoutLimit;
//		this.setMessageReceived(false);
//	}
//
//	@Override
//	public boolean connectUdp()
//	{
//		this.udpPort = udpPort;
//		
//		this.client = new Client();
//		client.start();
//		
//		try 
//		{
//			client.connect(timeoutLimit, this.url, NetworkCons.getTcpPort() ,NetworkCons.getUdpPort());
//		} 
//		catch (IOException e) 
//		{
//			e.printStackTrace();
//			return false;
//		}
//		
//		client.getKryo().register(PacketMessage.class);
//		
//		client.addListener(new Listener() {
//		       public void received (Connection connection, Object object) {
//		          if (object instanceof PacketMessage) {
//		        	 PacketMessage response = (PacketMessage)object;
//		             System.out.println("********** " +  response.message);
//		             setMessageReceived(true);
//		          }
//		       }
//		    });
//		
//		return true;
//	}
//
//	@Override
//	public void sendMessage(String message)
//	{
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void diconnect()
//	{
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public boolean connectTcp()
//	{
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//	public boolean isMessageReceived()
//	{
//		return isMessageReceived;
//	}
//
//	public void setMessageReceived(boolean messageReceived)
//	{
//		this.isMessageReceived = messageReceived;
//	}
//}