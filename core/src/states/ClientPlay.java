package states;

import handlers.GameStateManager;
import network.NetworkCons;

import com.badlogic.gdx.Gdx;


public class ClientPlay extends Play
{	
	protected ClientPlay(GameStateManager gsm)
	{
		super(gsm);
	}
	
	public void start()
	{
		String url = NetworkCons.getLocalhost();

		switch (Gdx.app.getType()) 
		{
		case Android:
			url = NetworkCons.getAndroidlocalhost();
			break;

		default:
			// Other platforms specific code
		}

		
		/*connection = new GdxClient(url, NetworkCons.getTimeout(), NetworkCons.getTcpPort());
		connection.connectTcp();
		messager = new Messager(connection, 0);*/
		
		super.start();
	}

	static public Play getFirstInstance(GameStateManager gsm)
	{
		if (thePlay == null )
			thePlay = new ClientPlay(gsm);
		return thePlay;
	}
}