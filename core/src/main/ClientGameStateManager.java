package main;

import handlers.GameStateManager;
import states.GameState;
import states.ClientPlay;

public class ClientGameStateManager extends GameStateManager
{	
	public ClientGameStateManager(Game game)
	{
		super(game);
	}

	protected GameState getState(int state)
	{
		if(state == PLAY)
			return ClientPlay.getFirstInstance(this);
		return null;
	}
}