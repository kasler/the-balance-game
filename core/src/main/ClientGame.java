package main;


import multiplayer.GdxClient;
import network.Connection;
import network.Messager;
import network.messages.BalanceMessage;
import network.messages.TestMessage;

public class ClientGame extends Game
{
	public void create()
	{
		super.create();
		Messager messager = new Messager(0);
		Connection connection = new GdxClient(messager);
		connection.connectTcp();
		BalanceMessage test = new TestMessage();
		connection.sendMessage(test);
		
		gsm = new ClientGameStateManager(this);
	}
}