package handlers;

import java.util.LinkedList;
import java.util.Queue;

import network.messages.BalanceMessage;

import com.badlogic.gdx.utils.TimeUtils;


public class Server
{
	private int i;
	private float pingMS;
	private Queue<BalanceMessage> messageQueue;

	public Server(float pingMS)
	{
		i = 0;
		this.pingMS = pingMS;
		this.messageQueue = new LinkedList<BalanceMessage>(); 
	}
	
	public void send(BalanceMessage msg)
	{
//		System.out.println("pushed " + i);
		msg.i = i;
		this.messageQueue.add(msg);
		++i;
	}
	
	public BalanceMessage recv()
	{
		BalanceMessage cur = null;
		
		if (!this.messageQueue.isEmpty() &&
				TimeUtils.timeSinceMillis(this.messageQueue.peek().time) > pingMS)
		{
			cur = this.messageQueue.poll();
//			System.out.println("poped " + cur.i);
		}
		
		return cur;
	}
}
